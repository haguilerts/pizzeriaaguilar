import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { timeout } from 'rxjs';
import { MisPizzas, Mypizza } from 'src/app/others/interface/myinterface';
import { ServicioService } from 'src/app/others/services/servicio.service';

@Component({
  selector: 'app-cardpizza',
  templateUrl: './cardpizza.component.html',
  styleUrls: ['./cardpizza.component.scss']
})
export class CardpizzaComponent {
  pizza: Mypizza[] | any
  cantTotal: number = 50
  precioTotal: number = 83600
  fecha:any
  nombre:string=""
  constructor(private servis: ServicioService) {
    this.pizza = [{
      id: 0,
      nombre: "muza",
      canVenta: 0,
      precio: 0,
      img: "../../../assets/img/muza.png",
      Descripcion: []
    }]
  }

  guardarBD(){
    
    if (this.fecha==undefined || null || this.nombre=="") {
      console.log("-------------no se envio nada")
      
    } else {
    //this.servis.post_guardarBD(this.nombre,this.fecha,this.cantTotal,this.precioTotal)
      
    }
  }

  async getAll_BD() {
    try {
      await this.servis.getAll_BBDD().subscribe(
        (items) => { this.pizza = items },
        async (Error: HttpErrorResponse) => {
          console.log("datos no encontrado")
          // this.servis.getOne_Storage(this.pizza[this.pizza.length].nombre)
          //await this.pizza.push( this.servis.getAll_Storage() ) 
          // await this.pizza.push( this.servis.getAll_Storage() ) 
          console.log(this.servis.getAll_Storage())
        }
      )
    } catch (e) {
      console.log('my error' + e);
    }
  }
  async ngOnInit() {
    try {
      this.pizza = await this.servis.getAll_Data();

    } catch (error) {
      console.error(error)
    }
    // this.calculoTotal()   
  }

  public selectSumar(i: any) {
    console.log(i.id)
    document.querySelector("#fila")?.children[i.id-1].classList.add("active")
    console.log(document.querySelector("#fila")?.children[i.id-1].classList)
    this.pizza[i.id - 1].canVenta = (i.canVenta) + 1 //añadir cantidad
    this.pizza[i.id - 1].subTotal = this.pizza[i.id - 1].canVenta * this.pizza[i.id - 1].precio //añadir Subtotal
    this.calculoTotal('suma');
    console.log("65".codePointAt(0))
   
    setTimeout(()=>{
      document.querySelector("#fila")?.children[i.id-1].classList.remove("active")
      console.log('borrado')
    },500)
  }
  public selectRestar(i: any) {
    console.log('resta', this.pizza[i.id - 1].canVenta)
    if (this.pizza[i.id - 1].canVenta != 0) {
      console.log('!=0')
      this.pizza[i.id - 1].canVenta = (i.canVenta) - 1 //añadir cantidad
      this.pizza[i.id - 1].subTotal = this.pizza[i.id - 1].canVenta * this.pizza[i.id - 1].precio //añadir Subtotal
      this.cantTotal = this.cantTotal - 1 // resta cantidad -1
      //this.precioTotal=this.precioTotal-
      this.calculoTotal('resta');
    } else {
      console.log('==0')
    }

  }
  public calculoTotal(operacion: string) {
    
    let resta = 0
    if (operacion = 'suma') {
      let sumCan = 0
      let sumTot = 0
      for (let i = 0; i < this.pizza.length; i++) {
        sumCan = sumCan + this.pizza[i].canVenta
        sumTot = sumTot + this.pizza[i].subTotal

      }
      this.cantTotal = sumCan
      this.precioTotal= sumTot
     
    } else {
      for (let i = 0; i < this.pizza.length; i++) {
       // suma = suma + this.pizza[i].canVenta
        this.precioTotal = this.precioTotal + this.pizza[i].subTotal

      }
    }


  }
  private postStorage(pizza: Mypizza) {
    this.servis.post_Storage(pizza)
    console.log('ok post')
  }



}
