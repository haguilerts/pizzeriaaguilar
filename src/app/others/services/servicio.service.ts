import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Mypizza, MisPizzas } from '../interface/myinterface';
import { producPizza } from '../BBDD/BD_localStorage';


@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  api: string = "https://sheet.best/api/sheets/5e24492d-1f44-4437-8dc3-58af4b17022b"

  constructor(private http: HttpClient) { }

  getAll_Data(): Promise<Mypizza[]> {
    return new Promise((resolve, rejects) => {
      resolve(producPizza)
    })
  }
  getAll_BBDD(): Observable<Mypizza[]> {
    return this.http.get<Mypizza[]>(this.api);
    //.get<Mypizza[]>(this.api).toPromise();
    console.log("api: " + this.api)
  }

  getAll_Storage(): Array<any> {
    var pizzas: Array<any> = []
    /*for(let i=0; i<localStorage.length; i++) {
      let key:any = ( localStorage.key(i));
      //console.log(key)
      console.log( localStorage.getItem(key) )
     pizzas.push( localStorage.getItem(key))
     // alert(`${key}: ${localStorage.getItem(key)}`);
    }*/
    for (let key in localStorage) {
      if (!localStorage.hasOwnProperty(key)) {
        continue; // se salta claves como "setItem", "getItem" etc
      }
      console.log(`${key}: ${typeof localStorage.getItem(key)} -- ${localStorage.getItem(key)}`);
    }
    console.log(pizzas)
    return pizzas;

  }
  getOne_Storage(i: any) {
    console.log(i)
    var key = '"' + i + '"'
    console.log(localStorage.getItem(key))
    // return null //localStorage.getItem(i)
  }

  post_Storage(pizza: Mypizza) {
    localStorage.setItem(JSON.stringify(pizza.nombre), JSON.stringify(pizza))
  }

  post_guardarBD(name:string,date:any,cant:number,precio:number){
    const data = {
      ID:null,
    	FECHA:date,
      NOMBRE:name,
      CANTIDAD:cant,
      SUBTOTAL:precio
    };
   console.log(data)
   try {
    fetch(this.api, {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((r) => r.json())
      .then((data) => {
        // The response comes here
        console.log(data);
      })
      .catch((error) => {
        // Errors are reported there
        console.log(error);
      });

    
  } catch (error) {
    
  }
  }

  update_Sum_Storage(pizza: Mypizza) {

  }
  update_Res_Storage() {

  }
  // getById(id:number):Promise<Mypizza>{
  //   return new Promise((resolve,rejects)=>{
  //     //resolve(Producto)
  //     try {
  //        Producto.filter(e=>{
  //         if(e.id==id){
  //           resolve(e)
  //         }
  //       })
  //     } catch (error) {
  //       console.log(error)
  //     }

  //   })


}

  // Escribir Filas
/*try {
    const respuesta = await fetch( api, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "Nombre": false,//formulario.nombre.value,
            "Correo": '"55"',//formulario.correo.value,
            "Telefono": 456//formulario.telefono.value
        })

    });
} catch (error) {
    console.log(error);
}
*/

 // Leer Filas
/* try {
     const respuesta = await fetch(api);

     const contenido = await respuesta.json();
     console.log("GET: BBDD")
     console.log(contenido);
 } catch (error) {
     console.log(error);
 }
*/
  // Eliminar Filas
/*try {
  const respuesta = await fetch(api+"/0", {
    method: 'DELETE'
  });

  const contenido = await respuesta.json();
      console.log("se Borro: ");
  console.log(contenido);
} catch(error){
  console.log(error);
}*/

  // Actualizar Filas
// try {
// 	const respuesta = await fetch(api+"/3", {
// 		method: 'PATCH',
// 		mode: 'cors',
// 		headers: {
// 			'Content-Type': 'application/json'
// 		},
// 		body: JSON.stringify({
// 			"Telefono": "1166015483"
// 		})
// 	});

// 	const contenido = await respuesta.json();
// 	console.log(contenido);
// } catch(error){
// 	console.log(error);
// }




