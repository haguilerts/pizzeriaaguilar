import { Mypizza } from "../interface/myinterface";

/* import{cards} from'../interfaces/cards'; */
export const producPizza: Mypizza[] = [
    {
        id: 1,          //01
        nombre: "muzarela",     //musa
        canVenta: 0,   //10 unidades  
        precio: 1500,       //$1500
        img: "../../../assets/img/muzarela.png",   
        subTotal:0,     //url
        Descripcion: []
    },
    {
        id: 2,         
        nombre: "Jamon",    
        canVenta: 0,  
        precio: 1700,      
        img: "../../../assets/img/Jamon.png",  
        subTotal:0,      
        Descripcion: []
    },
    
    {
        id: 3,         
        nombre: "Choclo",    
        canVenta: 0,  
        precio: 1700,      
        img: "../../../assets/img/Choclo.png",   
        subTotal:0,     
        Descripcion: []
    },
    {
        id: 4,         
        nombre: "Tomate",    
        canVenta: 0,  
        precio: 1700,      
        img: "../../../assets/img/Tomate.png",  
        subTotal:0,      
        Descripcion: []
    },
    {
        id: 5,         
        nombre: "Huevo",    
        canVenta: 0,  
        precio: 1700,      
        img: "../../../assets/img/Huevo.png",  
        subTotal:0,      
        Descripcion: []   
    },
    {
        id: 6,         
        nombre: "PAPFRITA",    
        canVenta: 0,  
        precio: 1700,      
        img: "../../../assets/img/PAPFRITA.png",  
        subTotal:0,      
        Descripcion: []
    },
    {
        id: 7,         
        nombre: "PEPERONI",    
        canVenta: 0,  
        precio: 1700,      
        img: "../../../assets/img/PEPERONI.png",  
        subTotal:0,      
        Descripcion: []   
    },
    //----------------- super----------------------

    {
        id: 8,         
        nombre: "J+T",    
        canVenta: 0,  
        precio: 1900,      
        img: "../../../assets/img/J+T.png",    
        subTotal:0,    
        Descripcion: []
    },
    {
        id: 9,         
        nombre: "J+H",    
        canVenta: 0,  
        precio: 1900,      
        img: "../../../assets/img/J+H.png",    
        subTotal:0,    
        Descripcion: []
    },
    {
        id: 10,         
        nombre: "J+CH",    
        canVenta: 0,  
        precio: 1900,      
        img: "../../../assets/img/J+CH.png",   
        subTotal:0,     
        Descripcion: []
    },
    {
        id: 11,         
        nombre: "J+PAPA",    
        canVenta: 0,  
        precio: 1900,      
        img: "../../../assets/img/J+PAPA.png",   
        subTotal:0,     
        Descripcion: []
    },
    {
        id: 12,         
        nombre: "T+CH",    
        canVenta: 0,  
        precio: 1900,      
        img: "../../../assets/img/T+CH.png",   
        subTotal:0,     
        Descripcion: []
    },
    //----------------- supremas----------------------
    {
        id: 13,         
        nombre: "J+H+T",    
        canVenta: 0,  
        precio: 2100,      
        img: "../../../assets/img/J+H+T.png",   
        subTotal:0,     
        Descripcion: []
    },
    {
        id: 14,         
        nombre: "J+CH+T",    
        canVenta: 0,  
        precio: 2100,      
        img: "../../../assets/img/J+CH+T.png",   
        subTotal:0,     
        Descripcion: []
    },
    {
        id: 15,         
        nombre: "J+H+CH",    
        canVenta: 0,  
        precio: 2100,      
        img: "../../../assets/img/J+H+CH.png",  
        subTotal:0,      
        Descripcion: []
    },
    //----------------- Torta ----------------------
    {
        id: 16,         
        nombre: "J+T+CH+H",    
        canVenta: 0,  
        precio: 2300,      
        img: "../../../assets/img/J+T+CH+H.png",  
        subTotal:0,
        Descripcion: []
    },
    {
        id: 17,         
        nombre: "J+T+CH+H+P",    
        canVenta: 0,  
        precio: 2500,      
        img: "../../../assets/img/J+T+CH+H+P.png",  
        subTotal:0,
        Descripcion: []
    },
];

