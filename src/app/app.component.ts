import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angularPizzeria';
}
/*
Component ng g component my-new-component

Directive ng g directive my-new-directive

Pipe ng g pipe my-new-pipe

Service ng g service my-new-service

Class ng g class my-new-class

Guard ng g guard my-new-guard

Interface ng g interface my-new-interface

Enum ng g enum my-new-enum

Module ng g module my-module
https://kaustubhtalathi.medium.com/angular-cli-part-ii-generating-components-directives-pipes-and-services-e8d914e32638
*/